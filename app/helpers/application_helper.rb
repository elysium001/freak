module ApplicationHelper
    
    def full_title( page_title = '')
        base_title = "Freak of JS, Bootstrap, Jquery, SCSS and CoffeeScript"
        if(page_title.empty?)
            base_title
        else
            "#{page_title} | #{base_title}"
        end
    end
end
